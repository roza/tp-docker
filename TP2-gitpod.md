# TP2

## Sans network

### On arrête et efface tous les containers

``` 
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
```

### On nettoie les images:

Ici nettoyons les images non taggées déjà:

```
docker rmi $(docker images -q -f dangling=true)
```

Si vous voulez effacer une image spécifique:

```
docker rmi image-id
```

Voyons ce que ça donne:
```
docker images
```

Ce qui devrait donner quelque chose du style:

```
REPOSITORY                TAG                 IMAGE ID            CREATED             SIZE

frolvlad/alpine-python3   latest              41b105c17525        13 days ago         56.7MB
mvertes/alpine-mongo      latest              6239a416ad9e        4 weeks ago         108MB
```

### Attention: on suppose une installation comme à l'IUT avec "Docker dans Docker "

Vous devez avoir à la racine de votre HOME un dossier **Docker** avec un fichiers PORTS à l'intérieur contenant la plage de ports dont vous disposez sur le serveur **servinfo-docker** 

### On crée l'arborescence suivante dans le dossier ~/Docker

```
todos
├── api
├── app
└── mongo
    └── db
```

### On lance mongodb

Ici 4000 = port public

```bash
docker run -d -p 4000:27017 -v /workspace/tp-docker/todos/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo
```
### On tape le code de Dockerfile et de run.py dans api et dans app

Au final doit avoir l'arborescence suivante :

```
todos
├── api
│   ├── Dockerfile
│   └── run.py
├── app
│   ├── Dockerfile
│   ├── run.py
│   └── templates
└── mongo
    └── db
```

Vérifiez bien le contenu de Docker/todos

En particulier les Dockerfile et run.py de api et de app

Pour lancer l'api "à la main ": 

```bash
docker run -ti --rm -p 4001:8082 -v /workspace/tp-docker/todos/api:/api --name todos-api frolvlad/alpine-python3 /bin/sh
```

avec 4101 = 2ème port utilisé

### On configure et on installe l'api dans le container

```bash
pip install flask flask-pymongo flask-restful
```

### On tape le code de run.py

### On lance 

```bash
    python3 /api/run.py
```

### On teste 

On ouvre un autre container (busybox ou alpine)  avec un terminal interactif :


On envoie une donnée à l'API dans ce container :

```bash
curl -X PUT http://todos-api:4001/roza -d '["un", "deux", "trois"]'
```

On vérifie qu'on peut la relire :

```bash
curl -X GET http://todos-api:4001/roza
```

4001= Votre port  réservé à l'API


### On complète le Dockerfile

```docker
FROM frolvlad/alpine-python3
RUN pip install flask flask-pymongo flask-restful
COPY run.py /api/run.py
EXPOSE 8082
CMD python3 /api/run.py
```

### On construit l'app 


### On build et run api et app

```
cd ~/Docker/todos/api
docker build -t roza/todos-api:v1 .
docker run -d  -p 4001:8082 -v /workspace/tp-docker/api --name todos-api roza/todos-api:v1
cd ~/Docker/todos/app
docker build -t roza/todos-app:v1 .
docker run -d -p 4002:7777 -v /workspace/tp-docker/app --name todos-app roza/todos-app:v1
```

Il est important de bien tagger les images pour s'y retrouver (:v1, v2, par exemple)


### Test dans un terminal d'un autre container

```sh
docker run --rm -ti busybox ...
```

On envoie une donnée à l'API:
```
curl -X PUT http://todos-api:4001/roza -d '["un","deux","trois"]'
```

Et on la relit :
```
curl -X GET http://todos-api:4001/roza
```

### Test de l'app dans un navigateur

Se connecter sur: http://todos-api:4002/roza


## Avec network

### On arrête les conatiners todos

```
docker stop todos-{app,api,mongo}
docker rm todos-{app,api,mongo}
```

### On crée un réseau local dédié

```
docker network create --driver bridge todos-net
docker network ls
```

Cela va nous éviter de publier les ports de todos-mongo et todos-api et de 'sortir' du container pour les faire communiquer.

### On relance mongodb en l'attachant à todos-net

```
docker run --network todos-net -d -v /workspace/tp-docker/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo
```

### Dans /Docker/todos/api/run.py

```python
app.config['MONGO_URI'] = 'mongodb://todos-mongo'
```

### Dans /Docker/todos/api/Dockerfile

Ajouter:

```
ENV no_proxy=todos-mongo
```

### Dans /Docker/todos/app/run.py

```python
API_URL = "http://todos-api:8082"
```

### Dans /Docker/todos/app/Dockerfile

On modifie:

```
ENV no_proxy=todos-api
```

### On rebuild  api et app

```
cd ~/workspace/tp-docker/todos/api
docker build -t roza/todos-api:v2 .
cd ~/workspace/tp-docker/todos/app
docker build -t roza/todos-app:v2 .
```

### On les relance attachées au réseau dédié

```
docker run --network todos-net -d  -v /workspace/tp-docker/api --name todos-api roza/todos-api:v2
docker run --network todos-net -d -p 4002:7777 -v /workspace/tp-docker/app --name todos-app roza/todos-app:v2
```

### On teste !!!


